DAT ProgramName         byte "WEEM's Security System", 0
CON
{{
  ****** Public Notes ******

  Code to demonstrate the object "Mfrc522_140321e".

  By: Duane Degn
  For: Dave Dorhout
  
  First attempt uploaded March 20, 2014.
  First working version uploaded March 21, 2014

  Version 140321e appears to read cards and tags
  correctly.

  The numbers listed in the "serialNumber" array
  down in the DAT section should be modified to
  match the numbers of your own cards and tags.

  The methods "PodBayDoorAction" and
  "SelfDestructAction" were added to the code
  as examples of using a code to start some
  sequence of code.
  
  For updated version of this code see the
  following thread in the Parallax Propeller
  Forum.

  http://forums.parallax.com/showthread.php/154851 

  See child object for additional credits.
  
}}
{
  ****** Private Notes ******

  Mf522Spi140319a Continue translating Arduino code
  to use sensor with Propeller.
  Change name to "Mfrc522Spi140319b".
  20c Still need a method to take the place of SPI.transfer.
  20c The code compiles. The program does not shift bits
  for SPI communication yet.
  20e SPI com needs to be msb first.
  20f Fix big CS problem with read.
  20h The local variable "backBits" wasn't
  being cleared before use in the
  "RequestFromMfrc522" method. Since only one byte of
  the variable was being written to, the other
  three bytes were interfering with the code.
  20h Appears to work correctly.
  20i Remove debug statements.
  20i Apparently I removed too much.
  20j Revert to h but use MainLoop from i.
  20j Appears to be working.
  21c Bug found in "Mfrc522ToCard" method. I needed to change
  a pair of "==" to "<>".
  21d Clean up code before posting to forum.
  21e Change name to "Mfrc522Demo140321e" in preparation
  to use this as a demo to the "Mfrc522_140321e" object.
  140321e Uploaded to forum.
  
}
CON

  _clkmode = xtal1 + pll16x 
  _xinfreq = 5_000_000

CON ' Pin Assignments

  SPI_CLK = 0
  SPI_MOSI = 4
  SPI_MISO = 2
  SPI_CS = 6
  RESET = 8
 
CON

  BAUD = 115_200 'initialize serial port number
  QUOTE = 34 'defining "
  END_OF_SERIAL_NUMBER_LIST = -1
 
  ' cardIndex enumeration
  #0, POD_BAY_DOOR_ACTION, SELF_DESTRUCT_ACTION
   
OBJ

  Pst : "Parallax Serial Terminal" 'loads Parallax Serial ports library
  Rfid : "Mfrc522_140321e" 'loads RFID scanner library
  
VAR

  long cardNumber, cardIndex
  long x, y, i
  byte serNum[Rfid#MAX_LEN]
  byte tempBuffer[Rfid#MAX_LEN]   
  
PUB Setup

  Pst.Start(BAUD)

  '' Comment out the five lines below to the have program immediately start.
  repeat
    Pst.Str(string(13, "Press any key to begin"))
    waitcnt(clkfreq / 4 + cnt)
    result := Pst.RxCount
  until result 'conditional compilation
  
  Pst.RxFlush

  Pst.Clear
  Pst.Home
  'Pst.Str(string(13, "Program Name = ")) 'the 13 is equivalent to enter
  Pst.Str(@ProgramName) 
  'Pst.Str(string(13, "RFID Object Name = ")) 
  'Pst.Str(Rfid.GetObjectName) 
  'Pst.Str(string(13, "MFRC522 SPI Interface Demonstation Program")) 
  Pst.Str(string(13,13, "Initializing RFID Reader", 13)) 
 
  Rfid.Init(SPI_MOSI, SPI_MISO, SPI_CLK, SPI_CS, RESET)
     
  DisplayReady
   
  MainLoop

PUB MainLoop | checksum

  repeat   
    result := Rfid.RequestFromMfrc522(Rfid#PICC_REQIDL, @serNum) 
     
    if result == Rfid#MI_OK
      'Pst.Str(string(13, "Card detected, type = $"))
      'Pst.Hex(serNum[0], 2)
      'Pst.Hex(serNum[1], 2)      
                                                       
      ' Anti-collision, return card serial number 4 bytes
      result := Rfid.Mfrc522Anticollision(@serNum)
     
      if result == Rfid#MI_OK
        result := Rfid.GetChecksum(@serNum)
        cardNumber := 0
        Rfid.SwapEndians(@cardNumber, @serNum)
       
        Pst.Str(string(13, "The card's number is  : $"))
        Pst.Hex(cardNumber, 8) 'grabs 8 bytes from card
        'Pst.Str(string(13, "Card's CRC value = $"))
        'Pst.Hex(serNum[4], 2)
        'Pst.Str(string(13, "Calculated checksum = $"))
        'Pst.Hex(result, 2)
        
        if result & $FF == serNum[4]
          Pst.Str(string(13, "Card Data Successfully Read"))
        else
          Pst.Str(string(13, "Warning CRC Error!!!"))
          next
        'Pst.Str(string(13, "Checking to see if card can be identified."))
        cardIndex := FindCardIndexNumber(cardNumber)
        if cardIndex == -1 'logic for unknown ID's
          Pst.Str(string(13, "****** Unknown card ******"))
          Pst.Str(string(13,13, "Please wait for security to arrive.",13))
          Pst.Hex(cardNumber, 8) 
                 
        else 'logic for registered ID's
          Pst.Str(string(13, "Hello, "))
          x:=cardNumber[0]
          'i:=0
          if x==serialNumber[2]
            Pst.Str(string("Wilson Kuang",13))
          if x==serialNumber[3]
            Pst.Str(string("Mason Fong",13))
          {
          case x
          serialNumber[2]:
           Pst.Str(string("Wilson Kuang",13))
          serialNumber[3]:
           Pst.Str(string("Mason Fong",13))
          }
         { 
          Pst.Str(string(13, "The card's corresponding text is ", QUOTE))
          Pst.Str(FindStringOrderPtr(@correspondingText, cardIndex))
          Pst.Char(QUOTE)
          Pst.Str(string(13, "Using the ", QUOTE, "case", QUOTE, " statement to branch"))
          Pst.Str(string(13, "the program based on which card was read."))
                   }
                   
                   
          case cardIndex
            POD_BAY_DOOR_ACTION:
              PodBayDoorAction
            SELF_DESTRUCT_ACTION:
              SelfDestructAction
              
            other:                                
              Pst.Str(string(13, "This card was recognized but no action has been "))
              Pst.Str(string(" associated with it."))
       
      else
        Pst.Str(string(13, "There was a problem reading from the card."))
        Pst.Str(string(13, "The returned error code = "))
        Pst.Dec(result)
        Pst.Str(string(13, "Please try again."))
        
        Pst.Str(string(13, "The data read = $"))
        repeat result from 0 to 4
        {
          Pst.Hex(serNum[result], 2)
          if result <> 4
            Pst.Str(string(", $"))
            }
      waitcnt(clkfreq / 2 + cnt)
      
      DisplayReady
        
PUB DisplayReady

  Pst.Str(string(13, 13, 13, "Ready to read card."))    

PUB PodBayDoorAction

  Pst.Str(string(13, "Open pod bay doors method."))
  'Pst.Char(7)
  waitcnt(clkfreq / 2 + cnt)
  'Pst.Char(7)
  waitcnt(clkfreq / 2 + cnt)
  'Pst.Char(7)
  waitcnt(clkfreq / 2 + cnt)  
  Pst.Str(string(13, "Sorry Dave I can't do that."))
  
PUB SelfDestructAction

  Pst.Str(string(13, "Self Destruct in:"))
  repeat result from 3 to 1
    Pst.Char(13)
    Pst.Char(13)
    Pst.Dec(result)
    waitcnt(clkfreq + cnt)

  Pst.Str(string(13, "****** BOOM ******"))
  repeat 10  
    'Pst.Char(7)
    waitcnt(clkfreq / 10 + cnt)

PUB FindStringOrderPtr(firstStr, stringIndex)     
'' Finds start address of one string in a list
'' of string. "firstStr" is the address of 
'' string #0 in the list. "stringIndex"
'' indicates which of the strings in the list
'' the method is to find.

  result := firstStr
  
  repeat while stringIndex    
    repeat while byte[result++]  
    stringIndex--


PUB FindCardIndexNumber(serialNumberToMatch)     
'' Finds the index number of card's serial number.
'' Used to find information associated with the
'' card.

  result := 0

  repeat while serialNumber[result] <> serialNumberToMatch and {
    } serialNumber[result] <> END_OF_SERIAL_NUMBER_LIST
    result++
  if serialNumber[result] == END_OF_SERIAL_NUMBER_LIST
    result := -1

PUB FindCardIndexAndText(serialNumberToMatch)
'' Displays text from DAT section corresponding to the
'' appropriate card.
'' Returns the card's index number (an easier to manage number
'' than the large serial numbers).
'' This method is not presently used.

  result := FindCardIndexNumber(serialNumberToMatch)
  if result == -1 
    Pst.Str(@notFoundText)
  else
    Pst.Str(FindStringOrderPtr(@correspondingText, result))
       
DAT
'' Add serial numbers from previously read cards.

serialNumber            long $739020BC '0
                        long $138DE88B '1
                        long $29D6F7C2 '2 fob
                        long $C596BD23 '3 card
                        long END_OF_SERIAL_NUMBER_LIST

'' The strings assigned under "correspondingText" need to be in the same order as
'' the order of the serial numbers listed above.
'' Make sure to terminate each string with a zero.
'' Use the method "FindCardIndexNumber" to find the index number of
'' a serial number read from a card. Use this index number with
'' the method "FindStringOrderPtr" to access the strings below.
'' See example in the method "MainLoop".

correspondingText       byte "Pod Bay Access", 0
                        byte "Initiate Self Destruct", 0
                       
notFoundText            byte "Unknown Card", 0                                                